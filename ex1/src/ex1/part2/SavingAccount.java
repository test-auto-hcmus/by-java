package ex1.part2;

public class SavingAccount {
	private long _amount;
	private int _term;
	private double _rate;
	
	public SavingAccount(long amount, int term, double rate) {
		this._amount = amount;
		this._term = term;
		this._rate = rate;
	}
	
	public long getAmount() {
		return _amount;
	}

	public int getTerm() {
		return _term;
	}

	public double getRate() {
		return _rate;
	}

	/*
	 * calculate the increasing amount after the term (number of months)
	 */
	public long calculateInterest() {
		throw new UnsupportedOperationException();
	}
}

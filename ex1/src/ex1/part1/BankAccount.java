package ex1.part1;

public class BankAccount {
	private String _name;
	private long _balance;
	
	public BankAccount(String name, long balance) {
		this._name = name;
		this._balance = balance;
	}
	
	public String getName() {
		return _name;
	}

	public long getBalance() {
		return _balance;
	}

	public void deposit(long amount) {
		throw new UnsupportedOperationException();
	}
	
	public void withdraw(long amount) {
		throw new UnsupportedOperationException();
	}
	
	public boolean canWithdraw(long amount) {
		throw new UnsupportedOperationException();
	}
}
